const nodemailer = require('nodemailer');

const config = require('../config');

const transporter = nodemailer.createTransport({
    host: config.email.smtp.host,
    port: config.email.smtp.port,
    secure: config.email.smtp.secure,
    auth: {
        user: config.email.user,
        pass: config.email.pass
    }
});

function send({ recipientName, recipientEmail, subject, text }) {
    const message = {
        from: `${config.email.senderName} <${config.email.senderEmail}>`,
        to: `${recipientName} <${recipientEmail}>`,
        subject,
        text
    };

    return new Promise((resolve, reject) => {
        transporter.sendMail(message, (err, info) => {
            if (err) {
                reject(err);
            } else {
                resolve(info);
            }
        });
    });
}

module.exports = {
    send
};

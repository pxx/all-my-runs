const winston = require('winston');
const Loggly = require('winston-loggly-bulk').Loggly;

const config = require('../config');

const defaultOptions = {
    silent: false
};

module.exports = (tag, options = defaultOptions) => {
    const logger = new winston.Logger({
        transports: [
            new Loggly(Object.assign({}, config.logger.loggly, { tags: [tag, `env-${process.env.NODE_ENV}`] }))
        ],
        exitOnError: false
    });

    // If we're not in production then log to the `console` with the format:
    if (process.env.NODE_ENV !== 'production' && !options.silent) {
        logger.add(winston.transports.Console, {
            level: 'debug',
            handleExceptions: true,
            timestamp: function () {
                return new Date().toISOString().slice(0, 19).replace('T', ' ');
            },
            formatter: (options) => {
                delete options.meta.timestamp;
                const level = winston.config.colorize(options.level, options.level.toUpperCase());
                const message = options.message ? options.message : '';
                const json = options.meta && Object.keys(options.meta).length
                    ? '\n\t' + JSON.stringify(options.meta)
                    : '';
                return `${options.timestamp()} ${level}\t${message}${json}`;
            }
        });
    }

    logger.info(`Start logging with tag: ${tag}`);

    return logger;
};

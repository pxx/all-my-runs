const resLogger = require('./logger')('amr-api-resources', { silent: true });

const config = require('../config');

const MEGABYTE = 1024 * 1024;

let usage = process.cpuUsage();
setInterval(() => {
    const memory = process.memoryUsage();
    usage = process.cpuUsage(usage);

    Object.keys(memory).forEach((key) => {
        memory[key] = memory[key] / MEGABYTE;
    });

    resLogger.info('', {
        cpu: (usage.system + usage.user) / 1000 / config.logger.resourcesPollingInterval * 100,
        memory
    });

    usage = process.cpuUsage();
}, config.logger.resourcesPollingInterval);

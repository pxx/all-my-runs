const restify = require('restify');
const { NotAuthorizedError } = require('restify-errors');
const corsMiddleware = require('restify-cors-middleware');
const config = require('./config');
const Token = require('./db/schemas/Token');
const anonRoutes = require('./routers/anonRoutes');

const server = restify.createServer(config.api.options);

// Cors
const cors = corsMiddleware({
    preflightMaxAge: 5, //Optional
    origins: ['*'],
    allowHeaders: ['Authorization'],
    exposeHeaders: []
});
server.pre(cors.preflight);
server.use(cors.actual);

server.use(restify.plugins.bodyParser({ mapParams: false }));
server.use(restify.plugins.authorizationParser());

server.use(async (req, res, next) => {
    if (anonRoutes.includes(req.getRoute().name)) {
        return next();
    } else if (req.authorization.scheme === 'Bearer') {
        const athlete = await Token.getAthleteByToken(req.authorization.credentials);
        if (!athlete) {
            return next(new NotAuthorizedError());
        } else {
            req.authorization.user = athlete.toObject().athlete;
            return next();
        }
    } else {
        return next(new NotAuthorizedError());
    }
});

// TODO: use own logger for db logging
server.on('restifyError', (req, res, error, callback) => {
    log.warn('API error', {
        statusCode: error.statusCode,
        method: req.method,
        url: req.url,
        requestHeaders: req.headers,
        remoteAddress: req.connection.remoteAddress,
        remotePort: req.connection.remotePort,
        error: error.body
    });

    return callback();
});
server.on('after', (req, res) => {
    if (res.statusCode >= 200 && res.statusCode <= 299) {
        log.info('API request', {
            statusCode: res.statusCode,
            method: req.method,
            url: req.url,
            requestHeaders: req.headers,
            responseHeaders: res._header,
            remoteAddress: req.connection.remoteAddress,
            remotePort: req.connection.remotePort
        });
    }
});

server.listen(config.api.port);

server.on('error', (error) => {
    log.error(error);
});

module.exports = server;

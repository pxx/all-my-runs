const testRouter = require('./test/test.router');
const userRouter = require('./user/user.router');
const queueRouter = require('./queue/queue.router');
const mapRouter = require('./map/map.router');
const activitiesRouter = require('./activities/activities.router');

[
    testRouter,
    userRouter,
    queueRouter,
    mapRouter,
    activitiesRouter
].forEach((router) => {
    router.applyRoutes(server, config.api.prefix);
});

log.info('API routes are registered');

const Token = require('../../db/schemas/Token');

async function saveToken(req, res, next) {
    try {
        const result = await Token.saveToken(req.body);
        res.send(200, result);
    } catch (error) {
        res.send(400, error);
    } finally {
        next();
    }
}

module.exports = saveToken;

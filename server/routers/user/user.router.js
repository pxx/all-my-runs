const { Router } = require('restify-router');
const userRouter = new Router();
const anonRoutes = require('../anonRoutes');

userRouter.post('/user/token', require('./saveToken'));
anonRoutes.push('postapiusertoken');

module.exports = userRouter;

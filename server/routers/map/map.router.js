const { Router } = require('restify-router');
const mapRouter = new Router();

mapRouter.get('/map', require('./getAthleteCombinedMap'));
mapRouter.get('/map/:activityType', require('./getAthleteCombinedMap'));

module.exports = mapRouter;

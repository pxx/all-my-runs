const Map = require('../../db/schemas/Map');

async function getAthleteCombinedMap(req, res, next) {
    try {
        const result = await Map.getAtheleteMaps(req.authorization.user.id, req.params.activityType);
        res.send(200, _combineMaps(result));
    } catch (error) {
        res.send(400, error);
    } finally {
        next();
    }
}

function _combineMaps(result) {
    return result.reduce((acc, item) => {
        return [...acc, ...item.latlng];
    }, []);
}

module.exports = getAthleteCombinedMap;

// Push here route names that should be accessible without authorization
// Name is generated in the following way: GET /test/:id -> 'getapitestid'
// Where 'api' part is a prefix for all api urls stored in config.api.prefix
module.exports = [];

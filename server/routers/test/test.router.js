const { Router } = require('restify-router');
const testRouter = new Router();

testRouter.get('/test', require('./getAll'));
testRouter.get('/test/:id', require('./getOneById'));
testRouter.post('/test', require('./post'));
testRouter.put('/test/:id', require('./put'));

module.exports = testRouter;

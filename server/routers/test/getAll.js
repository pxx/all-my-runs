const Test = require('../../db/schemas/Test');

async function getAll(req, res, next) {
    try {
        const result = await Test.find({});
        res.send(200, result);
    } catch (error) {
        res.send(400, error);
    } finally {
        next();
    }
}

module.exports = getAll;

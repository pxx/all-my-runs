const Test = require('../../db/schemas/Test');

async function getOneById(req, res, next) {
    try {
        const result = await Test.findById(req.params.id);
        res.send(200, result);
        // TODO: if (!result) { send error 404 }
    } catch (error) {
        // TODO: wrap error to make it standard
        res.send(400, error);
    } finally {
        next();
    }
}

module.exports = getOneById;

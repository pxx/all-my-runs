const Test = require('../../db/schemas/Test');

async function put(req, res, next) {
    try {
        const item = await Test.findById(req.params.id);
        Object.assign(item, req.body);
        const result = await item.save();
        res.send(200, result);
    } catch (error) {
        res.send(400, error);
    } finally {
        next();
    }
}

module.exports = put;

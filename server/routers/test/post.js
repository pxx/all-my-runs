const Test = require('../../db/schemas/Test');

async function post(req, res, next) {
    try {
        const test = new Test(req.body);
        const result = await test.save();
        res.send(200, result);
    } catch (error) {
        res.send(400, error);
    } finally {
        next();
    }
}

module.exports = post;

const Activity = require('../../db/schemas/Activity');

async function getAthleteCombinedMap(req, res, next) {
    try {
        const result = await Activity.getAggregatedTypes(req.authorization.user.id);
        res.send(200, result);
    } catch (error) {
        res.send(400, error);
    } finally {
        next();
    }
}

module.exports = getAthleteCombinedMap;

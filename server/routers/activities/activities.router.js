const { Router } = require('restify-router');
const activitiesRouter = new Router();

activitiesRouter.get('/activities/types-summary', require('./getTypesSummary'));

module.exports = activitiesRouter;

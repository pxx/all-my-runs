const QueueList = require('../../../db/schemas/QueueList');
const Task = require('../../../db/schemas/Task');

async function addAthleteList(req, res, next) {
    try {
        await Task.addTask(req.authorization.user.id);
        const result = await QueueList.addAthleteList(req.authorization.user.id);
        res.send(200, result);
    } catch (error) {
        res.send(400, error);
    } finally {
        next();
    }
}

module.exports = addAthleteList;

const QueueList = require('../../../db/schemas/QueueList');

async function findAthleteUnprocessedLists(req, res, next) {
    try {
        const result = await QueueList.findAthleteUnprocessedLists(req.authorization.user.id);
        res.send(200, result);
    } catch (error) {
        res.send(400, error);
    } finally {
        next();
    }
}

module.exports = findAthleteUnprocessedLists;

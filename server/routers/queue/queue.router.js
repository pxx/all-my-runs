const { Router } = require('restify-router');
const queueRouter = new Router();

queueRouter.get('/queue/list', require('./list/findAthleteUnprocessedLists'));
queueRouter.post('/queue/list', require('./list/addAthleteList'));

queueRouter.get('/queue/map', require('./map/findAthleteUnprocessedMaps'));

module.exports = queueRouter;

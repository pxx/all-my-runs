const QueueMap = require('../../../db/schemas/QueueMap');

async function findAthleteUnprocessedMaps(req, res, next) {
    try {
        const result = await QueueMap.findAthleteUnprocessedMaps(req.authorization.user.id);
        res.send(200, result);
    } catch (error) {
        res.send(400, error);
    } finally {
        next();
    }
}

module.exports = findAthleteUnprocessedMaps;

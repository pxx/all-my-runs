const mongoose = require('mongoose');
const dbLog = require('../services/logger')('amr-db');

const dbUri = `mongodb://${config.db.host}/${config.db.name}`;

mongoose.Promise = require('q').Promise;
mongoose.set('useFindAndModify', false);
dbLog.info(`Connecting to DB: ${dbUri}`);
mongoose.connect(dbUri, config.db.connectOptions);

const db = mongoose.connection;
db.on('error', (error) => {
    dbLog.error('DB error', error);
});
db.once('open', () => {
    dbLog.info(`DB connected`, {uri: dbUri});
});

module.exports = db;

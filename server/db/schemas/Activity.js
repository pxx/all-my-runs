const mongoose = require('mongoose');
const BaseSchema = require('./Base');

const ActivitySchema = new mongoose.Schema(Object.assign({}, BaseSchema, {
    _id: Number,
    athlete_id: { type: Number, required: true },
    start_date: { type: Date, required: true },
    activity: { type: Object, required: true }
}), {
    collection: 'activities'
});

ActivitySchema.statics.addActivity = function (payload) {
    const _payload = Object.assign({}, payload, {
        _id: payload.id,
        athlete_id: payload.athlete.id,
        start_date: payload.start_date,
        activity: payload
    });
    return this.findOneAndUpdate({ _id: payload.id }, _payload, { upsert: true, setDefaultsOnInsert: true });
};

ActivitySchema.statics.getAthleteLastActivityDate = function (athleteId) {
    return this
        .findOne({ athlete_id: athleteId }, 'start_date', { sort: { start_date: 'desc' } })
        .then((activity) => {
            return activity ? activity.start_date : null;
        });
};

ActivitySchema.statics.getAggregatedTypes = function (athleteId) {
    return this
        .aggregate([
            {
                $match: {
                    athlete_id: athleteId
                }
            },
            {
                $group: {
                    _id: '$activity.type',
                    count: { $sum: 1 },
                    time: { $sum: '$activity.elapsed_time' }
                }
            },
            {
                $sort: {
                    count: -1
                }
            }
        ]);
};

const Activity = mongoose.model('Activity', ActivitySchema);

module.exports = Activity;

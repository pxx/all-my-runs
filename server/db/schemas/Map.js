const mongoose = require('mongoose');
const BaseSchema = require('./Base');

const MapSchema = new mongoose.Schema(Object.assign({}, BaseSchema, {
    _id: Number,
    athlete_id: { type: Number, required: true },
    activity_type: { type: String, required: true },
    latlng: { type: [[Number]], default: [] }
}), {
    collection: 'maps'
});

MapSchema.statics.addMap = function (activityId, athleteId, activityType, latlng) {
    const payload = {
        _id: activityId,
        athlete_id: athleteId,
        activity_type: activityType,
        latlng
    };
    return this.findOneAndUpdate({ _id: activityId }, payload, { upsert: true, setDefaultsOnInsert: true });
};

MapSchema.statics.getAtheleteMaps = function (athleteId, activityType) {
    return activityType
        ? this.find({ athlete_id: athleteId, activity_type: activityType })
        : this.find({ athlete_id: athleteId });
};

const Map = mongoose.model('Map', MapSchema);

module.exports = Map;

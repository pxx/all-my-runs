const mongoose = require('mongoose');
const BaseSchema = require('./Base');

const TokenSchema = new mongoose.Schema(Object.assign({}, BaseSchema, {
    _id: Number,
    access_token: String,
    token_type: String,
    athlete: Object
}), {
    collection: 'tokens'
});

TokenSchema.statics.saveToken = function (payload) {
    return this
        .deleteMany({ _id: payload.athlete.id })
        .then(() => {
            const _payload = Object.assign({}, payload, {
                _id: payload.athlete.id
            });
            const token = new this(_payload);
            return token.save();
        });
};

TokenSchema.statics.getAthleteByToken = function (token) {
    return this
        .findOne({ access_token: token })
        .exec();
};

TokenSchema.statics.getAthleteById = function (athleteId) {
    return this
        .findById(athleteId)
        .exec();
};

const Token = mongoose.model('Token', TokenSchema);

module.exports = Token;

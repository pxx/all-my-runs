const mongoose = require('mongoose');
const BaseSchema = require('./Base');

const QueueMapSchema = new mongoose.Schema(Object.assign({}, BaseSchema, {
    athlete_id: { type: Number, required: true },
    processing: { type: Boolean, default: false },
    activity_id: { type: Number, required: true },
    processed_at: { type: Date, default: null }
}), {
    collection: 'queue_maps'
});

QueueMapSchema.statics.addMap = function (activityId, athleteId) {
    const payload = {
        athlete_id: athleteId,
        activity_id: activityId,
        processing: false,
        processed_at: null
    };
    const condition = {
        athlete_id: athleteId,
        activity_id: activityId
    };
    return this.findOneAndUpdate(condition, payload, { upsert: true, setDefaultsOnInsert: true });
};

QueueMapSchema.statics.findAthleteUnprocessedMaps = function (athleteId) {
    return this
        .find({ athlete_id: athleteId, processed_at: null })
        .sort({ created_at: 'asc' })
        .exec();
};

QueueMapSchema.statics.findAllUnprocessedMaps = function () {
    return this
        .find({ processed_at: null })
        .sort({ created_at: 'asc' })
        .exec();
};

QueueMapSchema.statics.findOneUnprocessedMap = function () {
    return this
        .findOneAndUpdate(
            { processed_at: null, processing: false },
            { processing: true },
            { sort: { created_at: 'asc' }, new: true }
        )
        .exec();
};


const QueueMap = mongoose.model('QueueMap', QueueMapSchema);
module.exports = QueueMap;

const mongoose = require('mongoose');
const BaseSchema = require('./Base');

const QueueListSchema = new mongoose.Schema(Object.assign({}, BaseSchema, {
    athlete_id: { type: Number, required: true },
    processing: { type: Boolean, default: false },
    processed_at: { type: Date, default: null },
    processed_params: { type: Object, default: null }
}), {
    collection: 'queue_lists'
});

QueueListSchema.statics.addAthleteList = function (athleteId) {
    const queueList = new this({ athlete_id: athleteId });
    return queueList.save();
};

QueueListSchema.statics.findAthleteUnprocessedLists = function (athleteId) {
    return this
        .find({ athlete_id: athleteId, processed_at: null })
        .sort({ created_at: 'asc' })
        .exec();
};

QueueListSchema.statics.findAllUnprocessedLists = function () {
    return this
        .find({ processed_at: null })
        .sort({ created_at: 'asc' })
        .exec();
};

QueueListSchema.statics.findOneUnprocessedList = function () {
    return this
        .findOneAndUpdate(
            { processed_at: null, processing: false },
            { processing: true },
            { sort: { created_at: 'asc' }, new: true }
        )
        .exec();
};


const QueueList = mongoose.model('QueueList', QueueListSchema);
module.exports = QueueList;

export interface BaseSchema {
    created_at: Date
}

export interface TokenSchema extends BaseSchema {
    _id: number,
    access_token: string,
    token_type: string,
    athlete: {}
}

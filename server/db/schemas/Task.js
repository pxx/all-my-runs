const mongoose = require('mongoose');
const BaseSchema = require('./Base');

const TaskSchema = new mongoose.Schema(Object.assign({}, BaseSchema, {
    athlete_id: { type: Number, required: true },
    processed_at: { type: Date, default: null }
}), {
    collection: 'tasks'
});

TaskSchema.statics.addTask = function (athleteId) {
    const condition = {
        athlete_id: athleteId,
        processed_at: null
    };
    return this.findOneAndUpdate(condition, condition, { upsert: true, setDefaultsOnInsert: true });
};

TaskSchema.statics.getAllPendingTasks = function () {
    return this
        .find({ processed_at: null })
        .sort({ created_at: 'asc' })
        .exec();
};


const Task = mongoose.model('Task', TaskSchema);
module.exports = Task;

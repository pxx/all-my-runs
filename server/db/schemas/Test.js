const mongoose = require('mongoose');
const BaseSchema = require('./Base');

const TestSchema = new mongoose.Schema(Object.assign({}, BaseSchema, {
    _id: Number,
    test: String,
    userid: Number
}));

TestSchema.pre('save', function (next) {
    if (!this._id) {
        this._id = this.userid;
    }
    // TODO: what if userid !== current _id?
    next();
});

const Test = mongoose.model('Test', TestSchema);

module.exports = Test;
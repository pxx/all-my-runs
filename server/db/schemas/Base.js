// TODO: switch to this solution if the following PR is merged
// https://github.com/briankircho/mongoose-schema-extend/pull/57
//
// require('mongoose-schema-extend');
// const BaseSchema = new mongoose.Schema({
//     created_at: { type: Date, required: true, default: Date.now }
// });
//
// Then use it the following way:
// const TokenSchema = BaseSchema.extend({ ... });

const BaseSchema = {
    created_at: { type: Date, required: true, default: Date.now }
};
module.exports = BaseSchema;

global.config = require('./config');
global.log = require('./services/logger')('amr-api');
global.db = require('./db/connector');

require('./services/resUsageLogger');

process.on('unhandledRejection', error => {
    log.error('unhandledRejection', error);
});

db.on('open', () => {
    if (process.env.API_CLEAN_DB === 'true') {
        cleanDB().then(runServer);
    } else {
        runServer();
    }
});

// TODO: remove it on production
async function cleanDB() {
    log.warn('Cleaning up DB');
    const QueueList = require('./db/schemas/QueueList');
    const QueueMap = require('./db/schemas/QueueMap');
    const Activity = require('./db/schemas/Activity');
    const Map = require('./db/schemas/Map');

    await QueueList.remove({}).exec();
    await QueueMap.remove({}).exec();
    await Activity.remove({}).exec();
    await Map.remove({}).exec();
}

function runServer() {
    global.server = require('./server');

    server.on('listening', () => {
        log.info(`${server.name} is listening at ${server.url}`);

        require('./routers/index.router');
        require('./daemons/processQueue');
        require('./daemons/completeTasks');
    });
}

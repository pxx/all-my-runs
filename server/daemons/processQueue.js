const moment = require('moment');
const strava = require('strava-v3');

const QueueList = require('../db/schemas/QueueList');
const QueueMap = require('../db/schemas/QueueMap');
const Activity = require('../db/schemas/Activity');
const Map = require('../db/schemas/Map');
const Token = require('../db/schemas/Token');

const MILLIS_15_MIN = 1000 * 60 * 15;
const MILLIS_DAY = 1000 * 60 * 60 * 24;
const TIMEOUT_SHORT = 20;
const TIMEOUT_LONG = 5000;

let timeout = TIMEOUT_SHORT;
let processingCount = 0;
// {"shortTermUsage":403,"shortTermLimit":600,"longTermUsage":403,"longTermLimit":30000}
let currentLimits = {};
let idleMessageShown = false;
let limitIdle = false;

step();

function step() {
    if (!limitIdle) {
        if (currentLimits.shortTermUsage === undefined && processingCount === 0) {
            getQueue();
        } else if (
            currentLimits.shortTermUsage
            && currentLimits.shortTermLimit - currentLimits.shortTermUsage - processingCount > 0
        ) {
            getQueue();
        } else if (currentLimits.shortTermUsage !== undefined) {
            limitReachedHandler();
        }
    }

    setTimeout(step, timeout);
}

async function getQueue() {
    processingCount += 1;
    const listQueueItem = await QueueList.findOneUnprocessedList();

    if (listQueueItem) {
        idleMessageShown = false;
        timeout = TIMEOUT_SHORT;
        const athlete = await Token.getAthleteById(listQueueItem.athlete_id);
        await processOneListQueueItem(listQueueItem, athlete.access_token);
    } else {
        const mapQueueItem = await QueueMap.findOneUnprocessedMap();

        if (mapQueueItem) {
            idleMessageShown = false;
            timeout = TIMEOUT_SHORT;
            const athlete = await Token.getAthleteById(mapQueueItem.athlete_id);
            await processOneMapQueueItem(mapQueueItem, athlete.access_token);
        } else {
            timeout = TIMEOUT_LONG;
            if (!idleMessageShown && !processingCount) {
                log.log('verbose', 'No unprocessed activities. Idling...');
                idleMessageShown = true;
            }
        }
    }
    processingCount -= 1;
}

async function processOneListQueueItem(listQueueItem, token) {
    const lastDate = await Activity.getAthleteLastActivityDate(listQueueItem.athlete_id);

    log.info(`Fetching activities from ${lastDate ? lastDate : 'the very beginning'}`);
    const payload = {
        access_token: token,
        per_page: config.strava.activitiesPerPage,
        after: lastDate ? moment(lastDate).unix() : 1
    };
    try {
        const activities = await fetchAthleteActivities(payload);
        await processActivities(activities);
        log.info(`Processed ${activities.length} activities`);

        if (activities.length === config.strava.activitiesPerPage) {
            await QueueList.addAthleteList(listQueueItem.athlete_id);
        }

        listQueueItem.processed_at = new Date();
        listQueueItem.processed_params = payload;
    } catch(errors) {
        if (_errorIsLimitReached(errors)) {
            limitReachedHandler();
        }
    } finally {
        listQueueItem.processing = false;
        await listQueueItem.save();
    }
}

function processActivities(activities) {
    const promises = activities.reduce((acc, activity) => {
        acc.push(Activity.addActivity(activity));
        acc.push(QueueMap.addMap(activity.id, activity.athlete.id));
        return acc;
    }, []);
    return Promise.all(promises);
}

async function processOneMapQueueItem(mapQueueItem, token) {
    log.info(`Fetching map ${mapQueueItem.activity_id}`);

    try {
        const stream = await fetchActivityStream(mapQueueItem.activity_id, token);

        const mapData = stream[0] && stream[0].data
            ? stream[0].data
            : [];

        const activity = await Activity.findById(mapQueueItem.activity_id);
        await Map.addMap(mapQueueItem.activity_id, mapQueueItem.athlete_id, activity.activity.type, mapData);

        mapQueueItem.processed_at = new Date();
    } catch (errors) {
        if (_errorIsLimitReached(errors)) {
            limitReachedHandler();
        }
    } finally {
        mapQueueItem.processing = false;
        await mapQueueItem.save();
    }
}

function fetchAthleteActivities(payload) {
    return new Promise((resolve, reject) => {
        strava.athlete.listActivities(payload, function (err, result, limits) {
            log.log('verbose', 'Strava API limits', limits);
            currentLimits = limits;

            if (err) {
                reject(err);
                log.warn('strava.athlete.listActivities error', err);
            } else if (result.errors && result.errors.length) {
                reject(result.errors);
                log.warn('strava.athlete.listActivities error', result);
            } else {
                resolve(result);
            }
        });
    });
}

function fetchActivityStream(activityId, token) {
    const payload = {
        access_token: token,
        id: activityId,
        types: 'latlng',
        resolution: 'low'
    };

    return new Promise((resolve, reject) => {
        strava.streams.activity(payload, (err, result, limits) => {
            log.log('verbose', 'strava API limits', limits);
            currentLimits = limits;

            if (err) {
                reject(err);
                log.warn('strava.streams.activity error', err);
            } else if (result.errors && result.errors.length) {
                reject(result.errors);
                log.warn('strava.streams.activity error', result);
            } else {
                resolve(result);
            }
        });
    });
}

function limitReachedHandler() {
    const shortLimit = currentLimits.shortTermLimit - currentLimits.shortTermUsage <= 0;
    const longLimit = currentLimits.longTermLimit - currentLimits.longTermUsage <= 0;

    const now = new Date().valueOf();
    let nextPeriod;

    if (longLimit) {
        nextPeriod = Math.ceil(now / MILLIS_DAY) * MILLIS_DAY;
    } else if (shortLimit) {
        nextPeriod = Math.ceil(now / MILLIS_15_MIN) * MILLIS_15_MIN;
    }

    const millis = nextPeriod - now;
    const time = moment(millis).utc().format('HH:mm:ss');

    // Object.assign({}, currentLimits, { idleTime: time })
    log.warn(`Limit reached. Idling...`, { ...currentLimits, idleTime: time });

    limitIdle = true;
    setTimeout(() => {limitIdle = false}, millis);
}

function _errorIsLimitReached(errors) {
    return errors
        && Array.isArray(errors)
        && errors.some((error) => error.field === 'rate limit' && error.code === 'exceeded');
}

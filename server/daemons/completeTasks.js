const mailer = require('../services/mailer');
const Task = require('../db/schemas/Task');
const QueueList = require('../db/schemas/QueueList');
const QueueMap = require('../db/schemas/QueueMap');

const MILLIS_1_MIN = 1000 * 60;

step();

function step() {
    getTasks();
    setTimeout(step, MILLIS_1_MIN);
}

async function getTasks() {
    try {
        // log.info('Checking ongoing tasks');
        const tasks = await Task.getAllPendingTasks();
        for (const task of tasks) {
            await processTask(task);
        }
    } catch (error) {
        log.error('Can\'t process tasks', error);
    }
}

async function processTask(task) {
    const listQueueItems = await QueueList.findAthleteUnprocessedLists(task.athlete_id);
    if (!listQueueItems.length) {
        const mapQueueItems = await QueueMap.findAthleteUnprocessedMaps(task.athlete_id);
        if (!mapQueueItems.length) {
            await completeTask(task);
        }
    }
}

async function completeTask(task) {
    try {
        task.processed_at = new Date();
        await task.save();
        log.info('Task complete', task.toObject());

        const mailResults = await sendMail(task);
        log.info('Mail sent', mailResults);
        // TODO: bulletproof mailing architecture
    } catch (error) {
        log.error(`Cannot complete task _id: ${task._id}`, error);
    }
}

function sendMail(task) {
    const message = {
        recipientName: 'Username',
        recipientEmail: 'email@user.com',
        subject: 'Your data is successfully processed',
        text: `Check it out${task.athlete_id}`
    };
    return mailer.send(message);
}

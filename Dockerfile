FROM mhart/alpine-node:8.15.0
MAINTAINER Andrey Prudnikov <a.prudnikov@gmail.com>

RUN apk add --no-cache git
RUN npm install -g yarn

RUN mkdir -p /opt/app
WORKDIR /opt/app

COPY package.json /opt/app/package.json
RUN yarn install

ADD . /opt/app

EXPOSE 8080

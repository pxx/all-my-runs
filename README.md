# My project's README

##NodeJS
Node-cron
https://github.com/kelektiv/node-cron


##MongoDB
Automatically backup a MongoDB database to S3 using mongodump, tar, and awscli (Ubuntu 14.04 LTS)
Raw
https://gist.github.com/eladnava/96bd9771cd2e01fb4427230563991c8d

Mongoose CRUD (Create, Read, Update, Delete)
https://coursework.vschool.io/mongoose-crud/


##Docker
Building Efficient Dockerfiles - Node.js
http://bitjudo.com/blog/2014/03/13/building-efficient-dockerfiles-node-dot-js/

Webpack and Docker for Development and Deployment
https://medium.com/@andyccs/webpack-and-docker-for-development-and-deployment-ae0e73243db4

10 Tips for Docker Compose Hosting in Production
http://blog.cloud66.com/10-tips-for-docker-compose-hosting-in-production/

Поднимаем сложный проект на Django с использованием Docker
https://habrahabr.ru/post/272811/

350+ полезных ресурсов, книг и инструментов для работы с Docker
https://habrahabr.ru/company/1cloud/blog/275015/


##Docker - AWS
Docker for AWS
https://www.docker.com/docker-aws

Полное практическое руководство по Docker: с нуля до кластера на AWS
https://habrahabr.ru/post/310460/

Deploying Node.js Microservices to AWS using Docker
https://community.risingstack.com/deploying-node-js-microservices-to-aws-using-docker/


##AWS
Введение в Amazon EC2 Container Service
https://habrahabr.ru/company/infopulse/blog/258657/

Опыт использования AWS ECS в нашей инфраструктуре
https://habrahabr.ru/post/311976/

EC2 Container Service Update – Container Registry, ECS CLI, AZ-Aware Scheduling, and More
https://aws.amazon.com/ru/blogs/aws/ec2-container-service-update-container-registry-ecs-cli-az-aware-scheduling-and-more/

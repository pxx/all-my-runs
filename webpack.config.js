const webpack = require('webpack');
const path = require('path');
const dotenv = require('dotenv');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const extractSass = new ExtractTextPlugin({
    filename: 'css/[name].[hash].css',
    disable: process.env.NODE_ENV === 'development'
});

const conf = {
    dir: {
        src: path.resolve(__dirname, 'client'),
        dist: path.resolve(__dirname, 'dist')
    }
};

// call dotenv and it will return an Object with a parsed key
const env = dotenv.config().parsed;

// reduce it to a nice object, the same as before
const envKeys = Object.keys(env).reduce((prev, next) => {
    prev[`process.env.${next}`] = JSON.stringify(env[next]);
    return prev;
}, {});

module.exports = {
    mode: 'development',
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx']
    },
    entry: {
        app: `${conf.dir.src}/index.tsx`,
        auth: `${conf.dir.src}/auth.ts`
    },
    devtool: 'inline-source-map',
    output: {
        filename: 'js/[name].[chunkhash].js',
        path: conf.dir.dist
    },
    module: {
        rules: [
            { test: /\.tsx?$/, loader: 'awesome-typescript-loader' },
            {test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel-loader'},
            // { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },
            // {test: /\.js$/, exclude: /node_modules/, loader: 'eslint-loader'},
            {
                test: /\.s?css$/, use: extractSass.extract({
                    use: [
                        {loader: 'css-loader'},
                        {loader: 'sass-loader'}
                    ],
                    fallback: 'style-loader'
                })
            },
            {
                test: /\.(png|jpg|jpeg|gif)$/,
                loader: 'file-loader',
                options: {
                    name: 'img/[name].[hash].[ext]'
                }
            },
            {
                test: /\.(svg|woff|woff2|ttf|eot)$/,
                loader: 'file-loader',
                options: {
                    name: 'font/[name].[hash].[ext]'
                }
            }
        ]
    },
    devServer: {
        port: 8080,
        contentBase: conf.dir.dist,
        historyApiFallback: true
    },
    plugins: [
        new webpack.DefinePlugin(envKeys),
        new CleanWebpackPlugin(conf.dir.dist),
        new HtmlWebpackPlugin({
            template: `${conf.dir.src}/auth.html`,
            filename: 'auth.html',
            chunks: ['auth']
        }),
        new HtmlWebpackPlugin({
            template: `${conf.dir.src}/index.html`,
            filename: 'index.html',
            chunks: ['app']
        }),
        extractSass
    ]
};

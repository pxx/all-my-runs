import {combineReducers} from 'redux';

import filtersReducer from './filters';
import mapReducer from './heatmap';
import queueReducer from './queue';

export default combineReducers({
    filters: filtersReducer,
    map: mapReducer,
    queue: queueReducer
});

import {AnyAction} from 'redux';
import {LatLngTuple} from 'leaflet';
import {MAP_FETCH_MAP_ERROR, MAP_FETCH_MAP_START, MAP_FETCH_MAP_SUCCESS} from '../actions/heatmap';

export interface HeatMapState {
    points: LatLngTuple[],
    fetchingMap: boolean
}

const defaultMap: HeatMapState = {
    points: [],
    fetchingMap: false
};

export default (state = defaultMap, action: AnyAction) => {
    switch (action.type) {
        case MAP_FETCH_MAP_START:
            return {
                ...state,
                fetchingMap: true
            };
        case MAP_FETCH_MAP_SUCCESS:
            return {
                ...state,
                points: action.payload,
                fetchingMap: false
            };
        case MAP_FETCH_MAP_ERROR:
            return {
                ...state,
                fetchingMap: false
            };
        default:
            return state;
    }
};

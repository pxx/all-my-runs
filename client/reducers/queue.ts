import { AnyAction } from 'redux';
import {
    QUEUE_ADD_QUEUE_ERROR,
    QUEUE_ADD_QUEUE_START, QUEUE_ADD_QUEUE_SUCCESS,
    QUEUE_FETCH_QUEUE_ERROR,
    QUEUE_FETCH_QUEUE_START,
    QUEUE_FETCH_QUEUE_SUCCESS
} from '../actions/queue';

export interface QueueState {
    list: any[] | null,
    map: any[] | null,
    fetchingQueue: boolean,
    addingQueue: boolean
}

const defaultQueue: QueueState = {
    list: null,
    map: null,
    fetchingQueue: false,
    addingQueue: false
};

export default (state = defaultQueue, action: AnyAction) => {
    switch (action.type) {
    case QUEUE_FETCH_QUEUE_START:
        return {
            ...state,
            fetchingQueue: true
        };
    case QUEUE_FETCH_QUEUE_SUCCESS:
        return {
            ...state,
            ...action.payload,
            fetchingQueue: false
        };
    case QUEUE_FETCH_QUEUE_ERROR:
        return {
            ...state,
            fetchingQueue: false
        };

    case QUEUE_ADD_QUEUE_START:
        return {
            ...state,
            addingQueue: true
        };
    case QUEUE_ADD_QUEUE_SUCCESS:
        return {
            ...state,
            addingQueue: false
        };
    case QUEUE_ADD_QUEUE_ERROR:
        return {
            ...state,
            addingQueue: false
        };
    default:
        return state;
    }
};

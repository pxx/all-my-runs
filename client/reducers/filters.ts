import {AnyAction} from 'redux';
import {FilterSummary} from '../interfaces';
import {
    FILTER_CHANGE,
    FILTERS_FETCH_SUMMARY_ERROR,
    FILTERS_FETCH_SUMMARY_START,
    FILTERS_FETCH_SUMMARY_SUCCESS
} from '../actions/filters';

export interface FiltersState {
    active: string,
    summary: FilterSummary[],
    fetchingSummary: boolean
}

const defaultFilters:FiltersState = {
    active: '',
    summary: [],
    fetchingSummary: false
};

export default (state = defaultFilters, action: AnyAction) => {
    switch (action.type) {
        case FILTER_CHANGE:
            return {
                ...state,
                active: action.payload
            };
        case FILTERS_FETCH_SUMMARY_START:
            return {
                ...state,
                fetchingSummary: true
            };
        case FILTERS_FETCH_SUMMARY_SUCCESS:
            return {
                ...state,
                fetchingSummary: false,
                summary: action.payload
            };
        case FILTERS_FETCH_SUMMARY_ERROR:
            return {
                ...state,
                fetchingSummary: false
            };
        default:
            return state;
    }
}

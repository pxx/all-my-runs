import axios from 'axios';
import config from '../config';

import { AnyObject } from '../interfaces';
import { goToLogin } from './authService';

const token: string = localStorage.getItem('auth:token');
axios.defaults.baseURL = config.api.baseUrl;
// eslint-disable-next-line dot-notation
axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;

function getQueueList() {
    const req = axios.get('/queue/list');
    return _handleUnauthorized(req);
}

function addQueueList() {
    const req = axios.post('/queue/list');
    return _handleUnauthorized(req);
}

function getQueueMap() {
    const req = axios.get('/queue/map');
    return _handleUnauthorized(req);
}

function getMap(activityType: string) {
    const req = axios.get(`/map/${activityType || ''}`);
    return _handleUnauthorized(req);
}

function getTypesSummary() {
    const req = axios.get('/activities/types-summary');
    return _handleUnauthorized(req);
}

function _handleUnauthorized(req: Promise<any>) {
    return req
        .catch((error: AnyObject) => {
            if (error.json.code === 'NotAuthorized') {
                goToLogin();
            }
        });
}

export default {
    queue: {
        list: {
            get: getQueueList,
            add: addQueueList
        },
        map: {
            get: getQueueMap
        }
    },
    map: {
        get: getMap
    },
    activities: {
        types: {
            get: getTypesSummary
        }
    }
};

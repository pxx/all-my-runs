import qs from 'qs';
import stravaConfig from '../../shared/strava.config';

export function goToLogin(): void {
    const query = qs.stringify({
        client_id: stravaConfig.client_id,
        redirect_uri: `${window.location.origin}/auth.html`,
        response_type: 'code',
        state: 'mystate',
        approval_prompt: 'force'
    });
    window.location.href = `https://www.strava.com/oauth/authorize?${query}`;
}

// import mockAxios from 'jest-mock-axios';
import api from './api';

describe('services/api', () => {
    // afterEach(() => {
    //     cleaning up the mess left behind the previous test
        // mockAxios.reset();
    // });

    it('', () => {
        let catchFn = jest.fn();
        let thenFn = jest.fn();

        api.queue.list.get()
            .then(thenFn)
            .catch(catchFn);

        expect(catchFn).toHaveBeenCalled();
    });
});

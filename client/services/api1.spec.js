import mockAxios from 'jest-mock-axios';
import api from './api1';

describe('services/api', () => {
    afterEach(() => {
        // cleaning up the mess left behind the previous test
        mockAxios.reset();
    });

    it('', () => {
        const catchFn = jest.fn();
        const thenFn = jest.fn();

        api.queue.list.get()
            .then(thenFn)
            .catch(catchFn);

        expect(mockAxios.get).toHaveBeenCalled();
    });
});

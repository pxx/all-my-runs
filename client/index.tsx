import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom';

import './index.scss';
import App from './components/App';
import Queue from './components/Queue';
import { goToLogin } from './services/authService';
import store from './store';

const token: string = localStorage.getItem('auth:token');
if (token) {
    // eslint-disable-next-line no-console
    console.log(`Seems to be authorized. Token: ${token}`);
} else {
    goToLogin();
}


const application = (
    <Provider store={store}>
        <Router>
            <div>
                <ul>
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/queue">Queue</Link></li>
                </ul>

                <hr/>

                <Route exact path="/" component={App}/>
                <Route path="/queue" component={Queue}/>
            </div>
        </Router>
    </Provider>
);


ReactDOM.render(application, window.document.getElementById('app'));

module.exports = {
    verbose: true,
    testURL: 'http://localhost',
    roots: [
        '<rootDir>'
    ],
    transform: {
        '^.+\\.tsx?$': 'ts-jest'
    },
    testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.[tj]sx?$',
    sourceType: 'module',
    moduleFileExtensions: [
        'ts',
        'tsx',
        'js',
        'jsx',
        'json',
        'node'
    ],
    modulePathIgnorePatterns: [
        '__mocks__'
    ],

    collectCoverageFrom: [
        '<rootDir>/**/*.{js,jsx,ts,tsx}',
        '!**/node_modules/**',
        '!**/vendor/**'
    ],

    // Setup Enzyme
    snapshotSerializers: ['enzyme-to-json/serializer'],
    setupTestFrameworkScriptFile: '<rootDir>/setupEnzyme.ts'
};

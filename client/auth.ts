import qs from 'qs';
import axios, { AxiosResponse } from 'axios';
import { TokenSchema } from '../server/db/schemas';
import config from './config';
import stravaConfig from '../shared/strava.config';

let code: string | null;

try {
    code = /code=([a-z0-9]+)/.exec(window.location.href)[1];
} catch (err) {
    code = null;
}

if (code) {
    axios.post('https://www.strava.com/oauth/token', qs.stringify({
        client_id: stravaConfig.client_id,
        client_secret: stravaConfig.client_secret,
        code
    }))
        .then((response: AxiosResponse) => axios.post(`${config.api.baseUrl}/user/token`, response.data))
        .then((response: AxiosResponse<TokenSchema>) => {
            localStorage.setItem('auth:token', response.data.access_token);
            window.location.href = '/';
        });
}

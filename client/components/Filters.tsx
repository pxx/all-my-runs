import React, { ChangeEvent } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import { filterChange, fetchFiltersSummary } from '../actions/filters';
import { FilterSummary } from '../interfaces';
import { FiltersState } from '../reducers/filters';
import { MyState } from '../actions';

interface FiltersProps {
    filters: FiltersState,
    filterChange: (filter: string) => void,
    fetchFiltersSummary: Function
}

class Filters extends React.Component<FiltersProps, any> {
    constructor(props: FiltersProps) {
        super(props);

        this.onSelectChange = this.onSelectChange.bind(this);

        this.props.fetchFiltersSummary();
    }

    onSelectChange(event: ChangeEvent<HTMLSelectElement>) {
        const element = (event.target);
        this.props.filterChange(element.value);
    }

    render() {
        const allTypes = '--- All Types ---';
        const filterElements = this.props.filters.summary
            .map((item: FilterSummary) => (
                <option key={item._id} value={item._id}>{`${item._id} (${item.count})`}</option>
            ));

        return (
            <div>
                Filters:
                <select onChange={this.onSelectChange}>
                    <option key={''} value={''}>{allTypes}</option>
                    {filterElements}
                </select>
            </div>
        );
    }
}

function mapStateToProps(state: MyState) {
    return {
        filters: state.filters
    };
}

function mapDispatchToProps(dispatch: Dispatch) {
    return bindActionCreators({
        filterChange,
        fetchFiltersSummary
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Filters);

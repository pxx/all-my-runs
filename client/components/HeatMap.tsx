import { LatLngTuple } from 'leaflet';
import React from 'react';
import { Map, TileLayer } from 'react-leaflet';
import HeatmapLayer from 'react-leaflet-heatmap-layer';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import config from '../config';
import { FiltersState } from '../reducers/filters';
import { HeatMapState } from '../reducers/heatmap';
import { fetchMap } from '../actions/heatmap';
import { MyState } from '../actions';


interface HeatMapProps {
    map: HeatMapState,
    filters: FiltersState,
    fetchMap: Function
}

class HeatMap extends React.Component<HeatMapProps, any> {
    constructor(props: HeatMapProps) {
        super(props);
    }

    componentDidUpdate(prevProps: HeatMapProps) {
        if (this.props.filters.active !== prevProps.filters.active) {
            this.updateData();
        }
    }

    componentDidMount() {
        this.updateData();
    }

    updateData() {
        this.props.fetchMap(this.props.filters.active);
    }

    render() {
        return (
            <Map id="mapid" center={config.map.start.position as LatLngTuple} zoom={config.map.start.zoom}>
                <HeatmapLayer
                    fitBoundsOnLoad
                    fitBoundsOnUpdate
                    points={this.props.map.points}
                    minOpacity={config.map.leaflet.minOpacity}
                    max={1}
                    radius={10}
                    blur={5}
                    latitudeExtractor={(m: number[]) => m[0]}
                    longitudeExtractor={(m: number[]) => m[1]}
                    intensityExtractor={() => 0.8}
                />
                <TileLayer
                    url='http://{s}.tile.osm.org/{z}/{x}/{y}.png?access_token={accessToken}'
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    accessToken={config.map.leaflet.accessToken}
                />
            </Map>
        );
    }
}

function mapStateToProps(state: MyState) {
    return {
        map: state.map,
        filters: state.filters
    };
}

function mapDispatchToProps(dispatch: Dispatch) {
    return bindActionCreators({
        fetchMap
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(HeatMap);

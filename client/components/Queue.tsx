import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import { MyState } from '../actions';
import { fetchQueue, addQueue } from '../actions/queue';
import { QueueState } from '../reducers/queue';

interface QueueProps {
    queue: QueueState,
    fetchQueue: Function,
    addQueue: Function
}

class Queue extends React.Component<QueueProps, any> {
    constructor(props: QueueProps) {
        super(props);

        this.addQueue = this.addQueue.bind(this);

        this.props.fetchQueue();
    }

    render() {
        return (
            <div>
                <h1>Queue</h1>
                <p>List requests: {this.props.queue.list !== null ? this.props.queue.list.length : '-'}</p>
                <p>Map requests: {this.props.queue.map !== null ? this.props.queue.map.length : '-'}</p>
                <p><button onClick={this.addQueue}>Add queue</button></p>
            </div>
        );
    }

    addQueue() {
        this.props.addQueue();
    }
}

function mapStateToProps(state: MyState) {
    return {
        queue: state.queue
    };
}

function mapDispatchToProps(dispatch: Dispatch) {
    return bindActionCreators({
        fetchQueue,
        addQueue
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Queue);

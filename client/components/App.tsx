import React from 'react';

import Filters from './Filters';
import Heatmap from './HeatMap';

class App extends React.Component {
    render() {
        return (
            <div>
                <h1>Hello</h1>
                <Filters />
                <Heatmap />
            </div>
        );
    }
}

export default App;

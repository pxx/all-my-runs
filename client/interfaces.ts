export interface AnyObject {
    [propName: string]: any
}

export interface FilterSummary {
    _id: string,
    count: number
}

import api from '../services/api';
import {Dispatch} from 'redux';

export const MAP_FETCH_MAP_START = 'MAP_FETCH_MAP_START';
export const MAP_FETCH_MAP_SUCCESS = 'MAP_FETCH_MAP_SUCCESS';
export const MAP_FETCH_MAP_ERROR = 'MAP_FETCH_MAP_ERROR';

export function fetchMap(filter: string) {
    return (dispatch: Dispatch) => {
        dispatch({ type: MAP_FETCH_MAP_START });
        api.map.get(filter)
            .then(({data}) => {
                dispatch({ type: MAP_FETCH_MAP_SUCCESS, payload: data });
            })
            .catch((error) => {
                dispatch({ type: MAP_FETCH_MAP_ERROR, payload: error, error: true });
            });
    }
}

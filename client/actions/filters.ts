import { AnyAction, Dispatch } from 'redux';

import api from '../services/api';
import { AnyObject } from '../interfaces';

export const FILTER_CHANGE = 'FILTER_CHANGE';
export const FILTERS_FETCH_SUMMARY_START = 'FILTERS_FETCH_SUMMARY_START';
export const FILTERS_FETCH_SUMMARY_SUCCESS = 'FILTERS_FETCH_SUMMARY_SUCCESS';
export const FILTERS_FETCH_SUMMARY_ERROR = 'FILTERS_FETCH_SUMMARY_ERROR';

export function filterChange(filter: string): AnyAction {
    return {
        type: FILTER_CHANGE,
        payload: filter
    };
}

export function fetchFiltersSummary() {
    return (dispatch: Dispatch) => {
        dispatch({ type: FILTERS_FETCH_SUMMARY_START });
        return api.activities.types.get()
            .then(({ data }) => {
                dispatch({ type: FILTERS_FETCH_SUMMARY_SUCCESS, payload: data });
            })
            .catch((error: AnyObject) => {
                dispatch({ type: FILTERS_FETCH_SUMMARY_ERROR, payload: error, error: true });
            });
    };
}

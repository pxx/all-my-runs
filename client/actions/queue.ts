import { AnyAction, Dispatch } from 'redux';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import { QueueState } from '../reducers/queue';
import api from '../services/api';

export const QUEUE_FETCH_QUEUE_START = 'QUEUE_FETCH_QUEUE_START';
export const QUEUE_FETCH_QUEUE_SUCCESS = 'QUEUE_FETCH_QUEUE_SUCCESS';
export const QUEUE_FETCH_QUEUE_ERROR = 'QUEUE_FETCH_QUEUE_ERROR';

export const QUEUE_ADD_QUEUE_START = 'QUEUE_ADD_QUEUE_START';
export const QUEUE_ADD_QUEUE_SUCCESS = 'QUEUE_ADD_QUEUE_SUCCESS';
export const QUEUE_ADD_QUEUE_ERROR = 'QUEUE_ADD_QUEUE_ERROR';

type QueueThunkDispatch = ThunkDispatch<QueueState, void, AnyAction>;
type QueueThunkAction = ThunkAction<void, QueueState, void, AnyAction>;

export function fetchQueue(): QueueThunkAction {
    return (dispatch: QueueThunkDispatch) => {
        dispatch({ type: QUEUE_FETCH_QUEUE_START });

        Promise.all([api.queue.list.get(), api.queue.map.get()])
            .then(([{ data: list }, { data: map }]) => {
                dispatch({ type: QUEUE_FETCH_QUEUE_SUCCESS, payload: { list, map } });
            })
            .catch((error) => {
                dispatch({ type: QUEUE_FETCH_QUEUE_ERROR, payload: error, error: true });
            });
    };
}

export function addQueue() {
    return (dispatch: QueueThunkDispatch) => {
        dispatch({ type: QUEUE_ADD_QUEUE_START });
        api.queue.list.add()
            .then(() => {
                dispatch({ type: QUEUE_ADD_QUEUE_SUCCESS });
                dispatch(fetchQueue());
            })
            .catch((error) => {
                dispatch({ type: QUEUE_ADD_QUEUE_ERROR, payload: error, error: true });
            });
    };
}

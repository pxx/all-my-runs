import {FiltersState} from '../reducers/filters';
import {HeatMapState} from '../reducers/heatmap';
import {QueueState} from '../reducers/queue';

export interface MyState {
    filters: FiltersState,
    map: HeatMapState,
    queue: QueueState
}

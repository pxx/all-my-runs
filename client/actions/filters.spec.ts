import mockAxios from 'jest-mock-axios';
import {fetchFiltersSummary, FILTER_CHANGE, filterChange} from './filters';

describe('Actions: filters', () => {
    it('filterChange', () => {
        const filter = 'SomeFilter';

        expect(filterChange(filter)).toEqual({
            type: FILTER_CHANGE,
            payload: filter
        });
    });

    // it('fetchFiltersSummary', () => {
    //     let catchFn = jest.fn();
    //     let thenFn = jest.fn();
    //
    //     fetchFiltersSummary()
    //         .then(thenFn)
    //         .catch(catchFn);
    // });
});

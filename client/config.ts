export default {
    api: {
        baseUrl: `//${process.env.API_HOST}:${process.env.API_PORT}/api`,
        list: {
            itemsPerPage: 200
        }
    },
    map: {
        start: {
            position: [51.505, -0.09],
            zoom: 2
        },
        leaflet: {
            minOpacity: 0.3,
            accessToken: 'pk.eyJ1IjoiYXBydWRuaWtvZmYiLCJhIjoiY2o2ZmRkbjdyMHU5MTJ3cW1xaHhxOGV6OCJ9.If22sVkjjn4MOsTbYZU9PQ'
        }
    }
};
